package com.raghuraman.blog.DTO;

public class CommentDTO {

	private String username;
	private String comment;
	private Integer blog_id;
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public CommentDTO() {
		super();
	}
	public CommentDTO(String username, String comment, Integer blog_id, Integer comment_id) {
		super();
		this.username = username;
		this.comment = comment;
		this.blog_id = blog_id;
		this.comment_id = comment_id;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	public Integer getBlog_id() {
		return blog_id;
	}
	public void setBlog_id(Integer blog_id) {
		this.blog_id = blog_id;
	}
	public Integer getComment_id() {
		return comment_id;
	}
	public void setComment_id(Integer comment_id) {
		this.comment_id = comment_id;
	}
	private Integer comment_id;
	
	
}
