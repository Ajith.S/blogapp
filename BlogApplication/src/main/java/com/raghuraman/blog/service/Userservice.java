package com.raghuraman.blog.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.raghuraman.blog.DTO.PostDTO;
import com.raghuraman.blog.entity.Blog;
import com.raghuraman.blog.entity.Comments;
import com.raghuraman.blog.entity.User;


@Service
@Transactional
public interface Userservice{


	List<User> getAllUsers();

	User getuserByName(String username);

	List<Blog> getuserposts(String username);
	 
	//List<Comments> getBlogComments(Integer blog_id);

	public List <Comments> getBlogComments(Integer post_id);
}
