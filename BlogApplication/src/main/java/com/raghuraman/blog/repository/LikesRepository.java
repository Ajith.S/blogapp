package com.raghuraman.blog.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.raghuraman.blog.entity.Comments;
import com.raghuraman.blog.entity.Likes;

public interface LikesRepository extends JpaRepository<Likes, Integer>  {

	List <Likes> findAllByUsername(String username);

	
	
}
