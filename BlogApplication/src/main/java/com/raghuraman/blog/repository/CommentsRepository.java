package com.raghuraman.blog.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.raghuraman.blog.entity.Blog;
import com.raghuraman.blog.entity.Comments;

public interface CommentsRepository extends JpaRepository<Comments, Integer>  {

	List<Comments> findAllByUsername(String username);

}
