package com.raghuraman.blog.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.raghuraman.blog.entity.Authorities;
import com.raghuraman.blog.entity.User;

@Repository
public interface AuthoritiesRepository extends JpaRepository<Authorities, Integer> {

}
