package com.raghuraman.blog.entity;


import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

@Entity
@Table(name="blog")

public class Blog {
	
	public Blog() {}
	

	public Blog( String blog_content) {
	
	
		this.blog_content = blog_content;
		
	}


	 @Id
	
	 @Column(name="post_id" )
	 private Integer post_id;

	 public Integer getPost_id() {
		return post_id;
	}
	 
	 public void  setPost_id(Integer post_id) {
		 
			this.post_id = post_id;
		}
	 
	
	
	

	 @OneToMany(fetch=FetchType.EAGER,mappedBy = "blogForComment", cascade = {CascadeType.REMOVE})
	  private List<Comments> comment;

	public List<Comments> getComment() {
		return comment;
	}


	public void setComment(List<Comments> comment) {
		this.comment = comment;
	}

	
	
	
	 @OneToMany(mappedBy = "blog",  cascade = {CascadeType.REMOVE})
	    private List<Likes> like;

		public List<Likes> getLike() {
			return like;
		}

		public void setLike(List<Likes> like) {
			this.like = like;
		}


	
//
//	@ManyToOne
//	@JoinColumn(name = "user_id")
//	 private User userId;
//
//	public User getuserId() {
//		return userId;
//	}
//
//	public void setuserId(User user_id) {
//		this.userId = user_id;
//	}


	
	    @ManyToOne()
	    @JoinColumn(name = "user_id")
	    private User user;

	public User getUser() {
			return user;
		}


		public void setUser(User user) {
			this.user = user;
		}


	@Column(name="blog")
	private String blog_content;

	public String getBlog_content() {
		return blog_content;
	}
	public void setBlog_content(String blog_content) {
		this.blog_content = blog_content;
	}

	






	

	
	
}